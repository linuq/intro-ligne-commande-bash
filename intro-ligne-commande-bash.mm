<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="BASH" FOLDED="false" ID="ID_503283942" CREATED="1542254350899" MODIFIED="1542337002181" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="7" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="HEADINGS"/>
<node TEXT="Pourquoi utiliser la ligne de commande?" POSITION="right" ID="ID_1956869423" CREATED="1542254362009" MODIFIED="1542254369999">
<edge COLOR="#ff0000"/>
<node TEXT="Automatiser des t&#xe2;ches" ID="ID_326356132" CREATED="1542254601389" MODIFIED="1542254607310">
<node TEXT="Cron" ID="ID_856458919" CREATED="1542258267678" MODIFIED="1542258272681"/>
</node>
<node TEXT="Effectuer une t&#xe2;che sp&#xe9;cifique" ID="ID_328312718" CREATED="1542254607829" MODIFIED="1542254628713">
<node TEXT="Manipulation d&apos;images et de vid&#xe9;os en lot" ID="ID_337154499" CREATED="1542258227934" MODIFIED="1542258247101"/>
<node TEXT="T&#xe9;l&#xe9;charger une archive d&apos;un site web" ID="ID_982926586" CREATED="1542258249750" MODIFIED="1542258261347"/>
<node TEXT="Synchroniser des fichiers" ID="ID_1520470516" CREATED="1542258591933" MODIFIED="1542258599950"/>
</node>
<node TEXT="Pouvoir reproduire une s&#xe9;quence d&apos;actions dans le futur" ID="ID_452505568" CREATED="1542254708360" MODIFIED="1542254721762">
<node TEXT="Script vs. logiciel" ID="ID_120199203" CREATED="1542258156765" MODIFIED="1542258164568"/>
</node>
<node TEXT="D&#xe9;velopper des logiciels" ID="ID_1618613236" CREATED="1542254629123" MODIFIED="1542254635001">
<node TEXT="GNU Make" ID="ID_509478653" CREATED="1542258138182" MODIFIED="1542258219552"/>
<node TEXT="Git" ID="ID_443329636" CREATED="1542258263338" MODIFIED="1542258264888"/>
</node>
<node TEXT="Comprendre le fonctionnement du syst&#xe8;me d&apos;exploitation" ID="ID_421705315" CREATED="1542254635387" MODIFIED="1542254647593">
<node TEXT="P&#xe9;riph&#xe9;riques" ID="ID_1427865900" CREATED="1542258169512" MODIFIED="1542258185443"/>
<node TEXT="Disques et m&#xe9;moire" ID="ID_772196116" CREATED="1542258185887" MODIFIED="1542258191111"/>
<node TEXT="Processus" ID="ID_556677499" CREATED="1542258192768" MODIFIED="1542258204019"/>
<node TEXT="Utilisateurs" ID="ID_230238696" CREATED="1542258208575" MODIFIED="1542258211813"/>
</node>
<node TEXT="Parce que parfois, on n&apos;a pas le choix" ID="ID_1863038923" CREATED="1542254652201" MODIFIED="1542254666130">
<node TEXT="Acc&#xe8;s &#xe0; distance avec SSH" ID="ID_297050150" CREATED="1542258611862" MODIFIED="1542258626295"/>
<node TEXT="Serveur Web" ID="ID_692406413" CREATED="1542258630201" MODIFIED="1542258634197"/>
</node>
<node TEXT="Standard &#xe9;tabli depuis des d&#xe9;cennies" ID="ID_1869573924" CREATED="1542254770983" MODIFIED="1542254795002">
<node TEXT="&#xc9;norm&#xe9;ment de documentation" ID="ID_54472101" CREATED="1542254801909" MODIFIED="1542254807963">
<node TEXT="Les pages &quot;man&quot;" ID="ID_1775898885" CREATED="1542255933673" MODIFIED="1542255941366"/>
<node TEXT="Une litt&#xe9;rature imposante" ID="ID_1380993538" CREATED="1542255942355" MODIFIED="1542255954664"/>
<node TEXT="Une tr&#xe8;s grande communaut&#xe9;" ID="ID_1738676244" CREATED="1542255955115" MODIFIED="1542255962368"/>
</node>
<node TEXT="Maturit&#xe9; des logiciels" ID="ID_1031689442" CREATED="1542254808342" MODIFIED="1542256677918"/>
<node TEXT="Ind&#xe9;pendance entre les distributions GNU/Linux et autres syst&#xe8;mes Unix" ID="ID_185217455" CREATED="1542254909649" MODIFIED="1542254957214"/>
</node>
</node>
<node TEXT="Comment ex&#xe9;cuter un programme?" POSITION="right" ID="ID_324230745" CREATED="1542254371930" MODIFIED="1542254391096">
<edge COLOR="#0000ff"/>
<node TEXT="L&apos;interface utilisateur, ou shell" ID="ID_43666482" CREATED="1542255029439" MODIFIED="1542256646708">
<node TEXT="Shebang" ID="ID_236379498" CREATED="1542260059890" MODIFIED="1542260063467"/>
</node>
<node TEXT="GNU Bash" ID="ID_193204652" CREATED="1542255815658" MODIFIED="1542256655172"/>
<node TEXT="Mac OS X et Windows" ID="ID_1416034709" CREATED="1542255818912" MODIFIED="1542256667415">
<node TEXT="Depuis Mac OS X Panther" ID="ID_741609338" CREATED="1542256868886" MODIFIED="1542256883828"/>
<node TEXT="Depuis Windows 10" ID="ID_834383986" CREATED="1542256884444" MODIFIED="1542256888573"/>
<node TEXT="Cygwin et Msys2" ID="ID_1062992195" CREATED="1542256895997" MODIFIED="1542256901392"/>
</node>
</node>
<node TEXT="Est-ce que GNU Bash est un vrai langage de programmation?" POSITION="right" ID="ID_185652977" CREATED="1542254391681" MODIFIED="1542255014412">
<edge COLOR="#00ff00"/>
<node TEXT="Structures de contr&#xf4;le" ID="ID_648615334" CREATED="1542258649273" MODIFIED="1542258657727"/>
<node TEXT="Variables" ID="ID_602338811" CREATED="1542258658126" MODIFIED="1542258661121"/>
<node TEXT="M&#xe9;ta-programmation" ID="ID_133643871" CREATED="1542258668395" MODIFIED="1542258673327"/>
</node>
<node TEXT="Quelles sont les diff&#xe9;rences entre GNU Bash et PowerShell?" POSITION="right" ID="ID_228913448" CREATED="1542254406904" MODIFIED="1542258694077">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="Comment travailler avec" POSITION="right" ID="ID_394102713" CREATED="1542254448544" MODIFIED="1542256462715">
<edge COLOR="#00ffff"/>
<node TEXT="Des fichiers" ID="ID_563962050" CREATED="1542256462720" MODIFIED="1542256467784"/>
<node TEXT="Du code source" ID="ID_318814066" CREATED="1542256469042" MODIFIED="1542256485401"/>
<node TEXT="Le r&#xe9;seau Internet" ID="ID_1229752491" CREATED="1542256488828" MODIFIED="1542256503285"/>
<node TEXT="Les processus" ID="ID_1612179489" CREATED="1542256505833" MODIFIED="1542256520010"/>
<node TEXT="Les permissions" ID="ID_557020816" CREATED="1542256522343" MODIFIED="1542256526563"/>
<node TEXT="Les utilisateurs" ID="ID_1800348938" CREATED="1542259572806" MODIFIED="1542259576997"/>
<node TEXT="La compression de fichiers" ID="ID_916136884" CREATED="1542256527145" MODIFIED="1542256545142"/>
<node TEXT="Quelques raccourcis utiles" ID="ID_42012899" CREATED="1542256558130" MODIFIED="1542256563902"/>
</node>
<node TEXT="Les expression r&#xe9;guli&#xe8;res" POSITION="right" ID="ID_247402193" CREATED="1542254457891" MODIFIED="1542256920604">
<edge COLOR="#7c0000"/>
<node TEXT="Essentielles pour tirer profit de la ligne de commande?" ID="ID_1924296437" CREATED="1542256921563" MODIFIED="1542256930082"/>
<node TEXT="Les utilitaires sed, awk et grep" ID="ID_1308473423" CREATED="1542258704959" MODIFIED="1542258720061"/>
</node>
<node TEXT="R&#xe9;f&#xe9;rences" POSITION="left" ID="ID_1335176055" CREATED="1542256785418" MODIFIED="1542256788151">
<edge COLOR="#00007c"/>
<node TEXT="Images" ID="ID_1394774937" CREATED="1542256789091" MODIFIED="1542256791005">
<node TEXT="https://www.pinterest.ca/pin/567523990538363374/" ID="ID_89393666" CREATED="1542256792373" MODIFIED="1542256796130"/>
<node TEXT="https://www.pinterest.ca/pin/607634174701601027/" ID="ID_554368858" CREATED="1542259618170" MODIFIED="1542259619879"/>
<node TEXT="https://www.pinterest.ca/pin/326511041729459939/" ID="ID_1714506178" CREATED="1542259698330" MODIFIED="1542259700575"/>
</node>
<node TEXT="Chaines Youtube" ID="ID_105891280" CREATED="1542259932790" MODIFIED="1542259937259">
<node TEXT="HakTip with Shannon Morse https://www.youtube.com/playlist?list=PLA2578BDE9CB9AAB3" ID="ID_989583339" CREATED="1542259938295" MODIFIED="1542259956312"/>
</node>
</node>
</node>
</map>
